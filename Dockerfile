FROM node:8.11.3-jessie

RUN mkdir /tas
WORKDIR /tas

ENV DEBIAN_FRONTEND noninteractive
ENV LANG C.UTF-8
ENV LANGUAGE C.UTF-8
ENV LC_ALL C.UTF-8
ENV RUBY_MAJOR 2.5
ENV RUBY_VERSION 2.5.1
ENV RUBY_DOWNLOAD_SHA256 886ac5eed41e3b5fc699be837b0087a6a5a3d10f464087560d2d21b3e71b754d
ENV RUBYGEMS_VERSION 2.7.7
ENV BUNDLER_VERSION 1.16.3

RUN apt-get update -qq
RUN apt-get install -y locales -qq
RUN locale-gen en_US.UTF-8 en_us
RUN dpkg-reconfigure locales
RUN dpkg-reconfigure locales
RUN locale-gen C.UTF-8
RUN /usr/sbin/update-locale LANG=C.UTF-8

# Install RUBY/GEM/BUNDLER
RUN mkdir -p /usr/local/etc
RUN { echo 'install: --no-document'; echo 'update: --no-document'; } >> /usr/local/etc/gemrc

# some of ruby's build scripts are written in ruby
# we purge system ruby later to make sure our final image uses what we just built
RUN set -ex \
  \
  && buildDeps=' \
    bison \
    dpkg-dev \
    libgdbm-dev \
    ruby \
  ' \
  && apt-get update \
  && apt-get install -y --no-install-recommends $buildDeps \
  && rm -rf /var/lib/apt/lists/* \
  \
  && wget -O ruby.tar.xz "https://cache.ruby-lang.org/pub/ruby/${RUBY_MAJOR%-rc}/ruby-$RUBY_VERSION.tar.xz" \
  && echo "$RUBY_DOWNLOAD_SHA256 *ruby.tar.xz" | sha256sum -c - \
  \
  && mkdir -p /usr/src/ruby \
  && tar -xJf ruby.tar.xz -C /usr/src/ruby --strip-components=1 \
  && rm ruby.tar.xz \
  \
  && cd /usr/src/ruby \
  \
  # hack in "ENABLE_PATH_CHECK" disabling to suppress:
  #   warning: Insecure world writable dir
  && { \
    echo '#define ENABLE_PATH_CHECK 0'; \
    echo; \
    cat file.c; \
  } > file.c.new \
  && mv file.c.new file.c \
  \
  && autoconf \
  && gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)" \
  && ./configure \
    --build="$gnuArch" \
    --disable-install-doc \
    --enable-shared \
  && make -j "$(nproc)" \
  && make install \
  \
  && apt-get purge -y --auto-remove $buildDeps \
  && cd / \
  && rm -r /usr/src/ruby \
  \
  && gem update --system "$RUBYGEMS_VERSION" \
  && gem install bundler --version "$BUNDLER_VERSION" --force \
  && rm -r /root/.gem/

# install things globally, for great justice
# and don't create ".bundle" in all our apps
ENV GEM_HOME /usr/local/bundle
ENV BUNDLE_PATH="$GEM_HOME"
ENV BUNDLE_SILENCE_ROOT_WARNING=1
ENV BUNDLE_APP_CONFIG="$GEM_HOME"
# path recommendation: https://github.com/bundler/bundler/pull/6469#issuecomment-383235438
ENV PATH $GEM_HOME/bin:$BUNDLE_PATH/gems/bin:$PATH
# adjust permissions of a few directories for running "gem install" as an arbitrary user

RUN mkdir -p "$GEM_HOME"
RUN chmod 777 "$GEM_HOME"

RUN gem install compass

RUN npm install -g \
  grunt@0.4.5 \
  webpack-cli@2.1.3 \
  bower \
  grunt-cli \
  yo \
  generator-angular

#---------------------------------#
# Prepare development environment #
#---------------------------------#
# Copy app into \tas dir

COPY . /tas

# Install dependencies
RUN bower install --allow-root
RUN npm install

EXPOSE 8081
EXPOSE 8080

CMD ["npm run startServers"]

# Windows Container
# FROM microsoft/nanoserver
# # Use powershell instead of cmd
# SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]
# # Add app working directory
# RUN md \tas
# WORKDIR \tas
# ENV RUBY_VERSION 2.5.0.1
# ENV BUNDLER_VERSION 1.16.1
# ENV NODE_VERSION 9.11.1
# ENV chocolateyUseWindowsCompression=false
# ENV NPM_CONFIG_LOGLEVEL info
# # # Install Chocolatey
# # RUN iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex ; \
# #     choco feature disable --name showDownloadProgress
# # Install Ruby & Bundler
# RUN iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex ; \
#     choco feature disable --name showDownloadProgress ; \
#     choco install -y ruby --version $env:RUBY_VERSION
# RUN gem install bundler --version $env:BUNDLER_VERSION --force ; \
#     gem update --system
# # Install Compass
# RUN gem install compass
# # Install Node & Yarn
# RUN choco install -y nodejs --version $env:NODE_VERSION
# # Install Git
# RUN choco install -y git
# # Install Grunt globally
# RUN npm install grunt -g
# #Install CMDR
# RUN choco install -y cmder
# #In the event we needed build tools
# #RUN npm install --global --vs2017 --production windows-build-tools
# #---------------------------------#
# # Prepare development environment #
# #---------------------------------#
# # Copy app into \tas dir
# COPY . \\tas
# # Install dependencies
# RUN npm i
# EXPOSE 8081
# EXPOSE 8080
# CMD [ "npm.cmd", "npm run startServers"]