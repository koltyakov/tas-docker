# TAS Suite in the Docker

## Build image

```bash
docker build -t tas-suite .
```

## Create container

```bash
docker run \
  --name=tas-dev \
  --hostname=localhost \
  -p 8080:8080 \
  -p 8081:8081 \
  -it tas-suite \
  node ./server
```

### Moount local folders

```bash
-v /local/folder:/folder/in/container
```

## Remove container

```bash
docker rm tas-dev
```